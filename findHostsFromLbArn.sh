#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Usage: $0 <load-balancer-arn> [region]"
    exit 1
fi

LOAD_BALANCER_ARN=$1
SPECIFIC_REGION="${2:-}"
TEMP_FILE=$(mktemp)

check_region_for_lb() {
    local region=$1
    local arn=$2
    local temp_file=$3
    if aws elbv2 describe-load-balancers --region "$region" --load-balancer-arns "$arn" &>/dev/null; then
        echo "$region" >> "$temp_file"
    fi
}

export -f check_region_for_lb
export LOAD_BALANCER_ARN
export AWS_DEFAULT_OUTPUT="text"

if [[ -n "$SPECIFIC_REGION" ]]; then
    check_region_for_lb "$SPECIFIC_REGION" "$LOAD_BALANCER_ARN" "$TEMP_FILE"
else
    regions=$(aws ec2 describe-regions --query "Regions[*].RegionName" --output text)
    for region in $regions; do
        (check_region_for_lb "$region" "$LOAD_BALANCER_ARN" "$TEMP_FILE") &
    done
    wait
fi

LB_REGIONS=$(sort -u "$TEMP_FILE")

check_hosted_zone() {
    local zone_id=$1
    local dns_name=$2
    local dns_name_lower=$(echo "$dns_name" | awk '{print tolower($0)}')

    aws route53 list-resource-record-sets --hosted-zone-id "$zone_id" \
        --query 'ResourceRecordSets[].[Type, Name, ResourceRecords[0].Value, AliasTarget.DNSName]' \
        --output text | while read -r type name value alias_dns; do

        if [[ -n "$alias_dns" ]]; then
            local alias_target_dns_lower=$(echo "$alias_dns" | awk '{print tolower($0)}')
            if [[ "$alias_target_dns_lower" == *"$dns_name_lower"* ]]; then
                echo "Alias found: $name"
            fi
        fi
    done
}

export -f check_hosted_zone

if [[ -n "$LB_REGIONS" ]]; then
    for region in $LB_REGIONS; do
        DNS_NAME=$(aws elbv2 describe-load-balancers --region "$region" --load-balancer-arns "$LOAD_BALANCER_ARN" --query "LoadBalancers[*].DNSName" --output text)
        echo "The load balancer specified was found in region $region as DNS $DNS_NAME"
        hosted_zones=$(aws route53 list-hosted-zones --query "HostedZones[*].Id" --output text)
        for zone_id in $hosted_zones; do
            (check_hosted_zone "$zone_id" "$DNS_NAME") &
        done
        wait
    done
else
    echo "The load balancer ARN specified was not found."
fi

rm -f "$TEMP_FILE"
