# AWS cli scripts
Scripts that wield aws-cli to make life easier
#

### findHostsFromLbArn &lt;load-balancer-arn&gt; [region]
Given a load balancer ARN and optional region, script searches hosted zones in route53 for matching alias.